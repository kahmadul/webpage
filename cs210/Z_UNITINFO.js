// CS 210
$( document ).ready( function() {

  unitInfo = {
  "Unit LOGIC" :
  {
    "link" : "unitLOGIC.html",
    "topic" : "Propositional Logic",
    "lectures" :
    [
      {
        "name" : "(Week 1) SPRING 2022 INTRODUCTION / SYLLABUS",
        "url" : "https://www.youtube.com/watch?v=zqqPpuqcxSU"
      },
      {
        "name" : "(Week 1) Lecture 1.1: Intro to Propositional Logic ",
        "url" : "https://www.youtube.com/watch?v=-lLpX6LAcWM"
      },
      {
        "name" : "(Week 1) Lecture 1.2: Conditional Statements (aka Implications)",
        "url" : "https://www.youtube.com/watch?v=3to0559TGYY"
      },
      {
        "name" : "(Week 2) Lecture 1.3: Predicates and Quantifiers",
        "url" : "https://www.youtube.com/watch?v=zcE0JD-DcPY"
      },
    ],
    "reading" :
    [
      {
        "name" : "(Week 1) 1.1: Introduction to Propositional Logic ",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter1-1-Intro_Propositional_Logic.pdf"
      },
      {
        "name" : "(Week 1) 1.2: Conditional Statements (aka Implications) ",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter1-2-Conditional_Statements.pdf"
      },
      {
        "name" : "(Week 2) 1.3: Introduction to Propositional Logic ",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter1-3-Predicates_and_Quantifiers.pdf"
      },
      {
        "name" : "(Week 2) 1.4: Conditional Statements (aka Implications) ",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter1-4-Laws_of_Propositional_Logic.pdf"
      },
      {
        "name" : "(Week 3) 1.5: Logical Reasoning ",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter1-5-Logical_Reasoning.pdf"
      },
    ],
    "exercises" :
    [
      {
        "key" : "INTRO", "name" : "(Week 1) 👋 The first assignment! ",
        "376" : "https://canvas.jccc.edu/courses/52456/modules/items/2475777",
        "due" : "Jan 24",
        "end" : "Jan 24",
      },
      {
        "key"   : "ULE1",
        "name"  : "(Week 1) Intro to Propositional Logic ",
        "docs"  : "docs/ULE1_Intro_to_Propositional_Logic",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2475779" ,
        "due"   : "Jan 25",
        "end"   : "Jan 28",
      },
      {
        "key"   : "ULE2",
        "name"  : "(Week 1) Conditional Statements ",
        "docs"  : "docs/ULE2_Conditional_Statements_(Implications).pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2475781" ,
        "due"   : "Jan 27",
        "end"   : "Jan 28",
      },
      {
        "key"   : "ULE3",
        "name"  : "(Week 2) Predicates and Quantifiers ",
        "docs"  : "docs/ULE3_Predicates_and_Quantifiers.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2475783" ,
        "due"   : "Feb 1",
        "end"   : "Feb 4",
      },
      {
        "key"   : "ULE4",
        "name"  : "(Week 2) Laws for Propositional Logic ",
        "docs"  : "docs/ULE4_Laws_for_Propositional_Logic.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2475785" ,
        "due"   : "Feb 3",
        "end"   : "Feb 4",
      },
      {
        "key"   : "C1", "name" : "(Week 2) Check-in ",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2584887",
        "due"   : "Jan 28",
        "end"   : "Jan 29",
      },
      {
        "key"   : "ULE5",
        "name"  : "(Week 3) Logical Reasoning ",
        "docs"  : "docs/ULE5_Logical_Reasoning.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2479355" ,
        "due"   : "Feb 8",
        "end"   : "Feb 11",
      },
      {
        "key"   : "ULTL",
        "name"  : "(Week 3) Propositional Logic and Computers ",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2475787" ,
        "due"   : "Feb 10",
        "end"   : "Feb 11",
      },
    ],
    "quizzes" :
    [
      {
        "key" : "ULQ1",
        "name"  : "(Week 1) Intro to Propositional Logic ",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2519566" ,
        "due"   : "Jan 25",
        "end"   : "Jan 28",
      },
      {
        "key" : "ULQ2",
        "name"  : "(Week 1) Conditional Statements ",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2520698" ,
        "due"   : "Jan 27",
        "end"   : "Jan 28",
      },
      {
        "key" : "ULQ3",
        "name"  : "(Week 2) Predicates and Quantifiers ",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2587343" ,
        "due"   : "Feb 1",
        "end"   : "Feb 4",
      },
    ],
    "tech-literacy" :
    [
    ],
    "exams" :
    [
      { "key" : "ULT", "name" : "Propositional Logic Test",
          "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2594669" ,
          "due" : "Feb 9",
          "end" : "Feb 9",
      },
    ],
  },

  "Unit PROOF1" :
  {
    "link"  : "unitPROOF1.html",
    "topic" : "Proofs (Part 1) <br> Introduction, Direct proofs",

    "lectures" :
    [
      {
        "name" : "Mathematical Writing (2016 lecture)",
        "url" : "http://lectures.moosader.com/cs210/CS210_06_Mathematical_Writing_old.mp4"
      },
    ],
    "reading" :
    [
      {
        "name" : "5.1: Properties of Numbers",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter5-1-Properties_of_Numbers.pdf"
      },
      {
        "name" : "5.2: Direct Proofs",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter5-2-Direct_Proofs.pdf"
      },
    ],

    "exercises" :
    [
      { "key" : "UPE1",
        "name"  : "Mathematical Writing (Tuesday)",
        "docs"  : "docs/UPE1_Mathematical_Writing.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2601241" ,
        "due"   : "Feb 16",
        "end"   : "Feb 18",
      },
      { "key" : "UPE2",
        "name"  : "Direct Proofs (Thursday)",
        "docs"  : "docs/UPE2_Direct_Proofs.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2602956" ,
        "due"   : "Feb 18",
        "end"   : "Feb 18",
      },
    ],

    "quizzes" :
    [
      { "key" : "UPQ1",
          "name"  : "Mathematical Writing (Tuesday)",
          "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2601296" ,
          "due"   : "Feb 16",
          "end"   : "Feb 18",
      },
    ]
  },

  "Unit SETS" :
  {
    "link"  : "unitSETS.html",
    "topic" : "Set Theory",

    "lectures" :
    [
      {
        "name" : "Set definitions and operations (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_11_Set_Definitions_and_Operations_old.mp4"
      },
      {
        "name" : "Cartesian Products, Power Sets, Partitions (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_12_More_Operations_on_Sets_old.mp4"
      },
    ],
    "reading" :
    [
      {
        "name" : "2.1: Introduction to Sets",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter2-1-Introduction_To_Sets.pdf"
      },
      {
        "name" : "2.2: Unions, Intersections, and Differences",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter2-2-Unions_Intersections_Differences.pdf"
      },
      {
        "name" : "2.3: Venn Diagrams",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter2-3-Venn_Diagrams.pdf"
      },
      {
        "name" : "2.4: Partitions",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter2-4-Partitions.pdf"
      },
      {
        "name" : "2.5: Cartesian Products",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter2-5-Cartesian_Products.pdf"
      },
      {
        "name" : "2.6: Power Sets",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter2-6-Power_Sets.pdf"
      },
    ],

    "exercises" :
    [
      {
        "key"   : "C2", "name" : "(Week 5) Check-in ",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2517843",
        "due"   : "Feb 18",
        "end"   : "Feb 28",
      },
      {
        "key"   : "USE1",
        "name"  : "(Week 6) Set basics",
        "docs"  : "docs/2022/USE1_Set_Basics.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609071" ,
        "due"   : "Mar 2",
        "end"   : "Mar 8",
      },
      {
        "key"   : "USE2",
        "name"  : "(Week 7) Partitions, power sets, and cartesian products",
        "docs"  : "docs/2022/USE2_Partitions_Power-Sets_Cartesian-Products.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609203" ,
        "due"   : "Mar 9",
        "end"   : "Mar 15",
      },
    ],
    "quizzes" :
    [
      {
        "key" : "USQ1",
        "name"  : "(Week 6) Introduction to Sets",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609229" ,
        "due"   : "Mar 2",
        "end"   : "Mar 8",
      },
      {
        "key" : "USQ2",
        "name"  : "(Week 6) Unions, Intersections, and Differences",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609294" ,
        "due"   : "Mar 2",
        "end"   : "Mar 8",
      },
      {
        "key" : "USQ3",
        "name"  : "(Week 7) Partitions",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609310" ,
        "due"   : "Mar 9",
        "end"   : "Mar 15",
      },
      {
        "key" : "USQ4",
        "name"  : "(Week 7) Cartesian Products",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609311" ,
        "due"   : "Mar 9",
        "end"   : "Mar 15",
      },
      {
        "key" : "USQ5",
        "name"  : "(Week 7) Power Sets",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2609312" ,
        "due"   : "Mar 9",
        "end"   : "Mar 15",
      },
    ],
    "exams" :
    [
      {
        "key" : "UST", "name" : "Sets Test",
        "376"   : "" ,
        "due" : "",
        "end" : "",
      },
    ],
  },

  "Unit PROOF2" :
  {
    "link"  : "unitPROOF2.html",
    "topic" : "Proofs (Part 2) <br> Proof by Contrapositive, Proof by Contradiction",

    "lectures" :
    [
      {
        "name" : "Proof by Contradiction (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_09_Proof_by_Contradiction_old.mp4"
      },
    ],
    "reading" :
    [
      {
        "name" : "Proof by Contrapositive",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter5-3-Proof_By_Contrapositive.pdf"
      },
      {
        "name" : "Proof by Contradiction",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter5-4-Proof_By_Contradiction.pdf"
      },
    ],

    "exercises" :
    [
      {
        "key"   : "UPE3", "name" : "Proof by Contrapositive",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2622146",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UPE3_Proof_by_Contrapositive.pdf",
        "due"   : "March 16",
        "end"   : "March 23",
      },
      {
        "key"   : "UPE4", "name" : "Proof by Contradiction",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2622147z",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UPE4_Proof_by_Contradiction.pdf",
        "due"   : "March 16",
        "end"   : "March 23",
      },
    ],
  },

  "Unit BOOL" :
  {
    "link"  : "unitBOOL.html",
    "topic" : "Boolean Algebra",

    "lectures" :
    [
      {
        "name" : "Intro to Boolean Algebra",
        "url" : "http://lectures.moosader.com/cs210/CS210_13b_Boolean_Algebra.mp4"
      },
      {
        "name" : "Logic Circuits and Karnaugh Maps",
        "url" : "http://lectures.moosader.com/cs210/CS210_14_Logic_Circuits_old.mp4"
      },
    ],
    "reading" :
    [
      {
        "name" : "Boolean Algebra",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter3-Boolean_Algebra.pdf"
      },
    ],

    "exercises" :
    [
      {
        "key"   : "UBE1", "name" : "Boolean Algebra",
        "docs"  : "docs/2022/UBE1_Boolean_Algebra.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2628581",
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
      {
        "key"   : "UBE2", "name" : "Logic Circuits",
        "docs"  : "docs/2022/UBE2_Logic_Circuits.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2628582",
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
      {
        "key"   : "UBE3", "name" : "Karnaugh Maps",
        "docs"  : "docs/2022/UBE3_Karnaugh_Maps.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2628583",
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
      {
        "key"   : "UBTL",
        "name"  : "Boolean Algebra and Logic Circuits",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629320" ,
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
    ],
    "quizzes" :
    [
      {
        "key" : "UBQ1",
        "name"  : "Boolean Algebra",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629244" ,
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
      {
        "key" : "UBQ2",
        "name"  : "Logic Circuits",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629294" ,
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
      {
        "key" : "UBQ3",
        "name"  : "Karnaugh Maps",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629303" ,
        "due"   : "Apr 6",
        "end"   : "Apr 13",
      },
    ],
    
    "exams" :
    [
      { "key" : "UBT", "name" : "Boolean Logic Test",
          "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2642792" ,
          "due" : "Apr 11",
          "end" : "Apr 11",
      },
    ],
  },

  "Unit PROOF3" : {
    "link"  : "unitPROOF3.html",
    "topic" : "Proofs (Part 3) <br> Mathematical Induction",

    "lectures" :
    [
      {
        "name" : "Mathematical Induction (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_07_Mathematical_Induction_old.mp4"
      },
      {
        "name" : "More with Induction (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_08_More_About_Induction_old.mp4"
      },
    ],
    "reading" :
    [
      {
        "name" : "Inductive proofs",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter5-5-Induction.pdf"
      },
    ],

    "exercises" :
    [
      {
        "key"   : "UPE5", "name" : "Inductive proofs",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629416",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UPE5_Induction.pdf",
        "due"   : "Apr 18",
        "end"   : "Apr 25",
      },
    ],
  },

  "Unit FUNC" : {
    "link"  : "unitFUNC.html",
    "topic" : "Functions and Relations",

    "lectures" :
    [
      {
        "name" : "Intro to Functions and Relations (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_4-1_Intro_to_Functions_and_Relations.mp4"
      },
      {
        "name" : "Function compositions (2016 video)",
        "url" : "http://lectures.moosader.com/cs210/CS210_4-2_Function_Compositions.mp4"
      },
    ],
    "reading" :
    [
      {
        "name" : "Functions and Relations",
        "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/standalone-chapter4-Functions_and_Relations.pdf"
      },
    ],
    "exams" :
    [
      { "key" : "UFT", "name" : "Functions and Relations Test",
          "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2657666" ,
          "due" : "May 6",
          "end" : "May 6",
      },
    ],

    "exercises" :
    [
      {
        "key"   : "UFE1", "name" : "Definitions, Diagrams, and Inverses",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UFE1_Definitions_Diagrams_Inverses.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629843",
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key"   : "UFE2", "name" : "Function Composition",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UFE2_Function_Composition.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629844",
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key"   : "UFE3", "name" : "Properties of Functions",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UFE3_Properties_of_Functions.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629845",
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key"   : "UFE4", "name" : "Properties of Relations",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UFE4_Properties_of_Relations.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629846",
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key"   : "UFE5", "name" : "Equivalence Relations",
        "docs"  : "https://gitlab.com/rachels-courses/archive_discrete-structures/-/blob/master/2022/Exercises/UFE5_Equivalence_Relations.pdf",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629847",
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key"   : "UFTL",
        "name"  : "Functions and Relations",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629870" ,
        "due"   : "May 10",
        "end"   : "May 11",
      },
    ],
    
    "quizzes" :
    [
      {
        "key" : "UFQ1",
        "name"  : "Definitions, Diagrams, and Inverses",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629866" ,
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key" : "UFQ2",
        "name"  : "Function Composition",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629867" ,
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key" : "UFQ3",
        "name"  : "Properties of Functions",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629868" ,
        "due"   : "May 10",
        "end"   : "May 11",
      },
      {
        "key" : "UFQ4",
        "name"  : "Properties of Relations",
        "376"   : "https://canvas.jccc.edu/courses/52456/modules/items/2629869" ,
        "due"   : "May 10",
        "end"   : "May 11",
      },
    ]
  },



    //"Unit 2" : {
      //"link" : "unit02.html",
      //"topic" : "Proofs, Part 1: ",
      //"exercises" :       [
      //],
    //},

    //"Unit 3" : {
      //"link" : "unit03.html",
      //"topic" : "Set Theory",
    //},

    //"Unit 4" : {
      //"link" : "unit04.html",
      //"topic" : "Proofs, Part 2: Proof by Contrapositive and Proof by Contradiction",
    //},

    //"Unit 5" : {
      //"link" : "unit05.html",
      //"topic" : "Boolean Algebra",
    //},

    //"Unit 6" : {
      //"link" : "unit06.html",
      //"topic" : "Proofs, Part 3: Mathematical Induction",
    //},

    //"Unit 7" : {
      //"link" : "unit07.html",
      //"topic" : "Functions and Relations",
    //},

  };

} );
