// CS 235
$( document ).ready( function() {

    headerInfo = {
        "course"        : "CS 235: Object Oriented Programming with C++",
        "dates"         : "2022-01-18 to 2022-05-16",
        "semester"      : "Spring 2022",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs235/",
        "textbook1"     : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_235",
        "syllabus"      : "syllabus.html",
        "exampleCode"   : "https://gitlab.com/rachels-courses/example-code/-/tree/main/C%2B%2B",
        "prerequisites" : "CS 200 or CS 201 or CS 205.",
        "description"   : "This course emphasizes programming methodology and problem solving using the object-oriented paradigm. Students will develop software applications using the object-oriented concepts of data abstraction, encapsulation, inheritance, and polymorphism. Students will apply the C++ techniques of dynamic memory, pointers, built-in classes, function and operator overloading, exception handling, recursion and templates. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
        "credit_hours"  : "4",
    };
    
    sectionInfo = [
        {
            "semester"      : "Spring 2022",
            "crn"           : "11486",
            "section"       : "350",
            "method"        : "Online only",
            "times"         : "Online only",
            "background"    : "bg1",
            "room"          : "none"
        },
        //{
            //"semester"      : "Fall 2021",
            //"crn"           : "82836",
            //"section"       : "351",
            //"method"        : "Online only",
            //"times"         : "Online only",
            //"background"    : "bg2"
        //},
    ];
    
} );
