// CS 235
$( document ).ready( function() {

unitInfo = {
  "Unit 1" : 
  {
      "link" : "unit01.html",
      "topic" : "Introduction and setup",
      "reminders" : [
          "If you have trouble setting anything up, send me an email via Canvas! We can work together via email or schedule a Zoom meeting to step through everything."
      ],
      "reading" :
      [
      ],
      "lectures" :        
      [
        { 
          "name" : "SPRING 2022 INTRODUCTION / SYLLABUS", 
          "url" : "https://youtu.be/F-_Im-lzRj4" 
        },
      ],
      "exercises" :
      [
        {
          "key" : "INTRO", "name" : "👋 The first assignment!",
          "350" : "https://canvas.jccc.edu/courses/52475/assignments/1062407",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },
        {
          "key" : "SETUP1", "name" : "Installing an IDE",
          "docs" : "ASSIGNMENT_SETUP1_IDE.html",
          "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2462080",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },
        {
          "key" : "SETUP2", "name" : "Creating a GitLab account",
          "docs" : "ASSIGNMENT_SETUP2_GitLab.html",
          "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2462081",
          "due" : "Jan 21",
          "end" : "Jan 21",
        },
        {
          "key" : "SETUP3", "name" : "Installing Git",
          "docs" : "ASSIGNMENT_SETUP3_Git.html",
          "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2462082",
          "due" : "Jan 24",
          "end" : "Jan 24",
        },
      ],
      "tech-literacy" :
      [
        { "key" : "TL1", "name" : "How compiling works in C++",
            "docs" : "ASSIGNMENT_TL1_CompilerLinker.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2462084",
            "due" : "Jan 26",
            "end" : "Jan 26",
        },
      ],
  },

  "Unit 2" : 
  {
      "link" : "unit02.html",
      "topic" : "Review: C++ Basics",

      "quizzes" :         
      [   
        { "key" : "R1", "name" : "C++ Review - Program basics",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2494682",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R2", "name" : "C++ Review - Control flow",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2494681",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R3", "name" : "C++ Review - Functions",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2494683",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R4", "name" : "C++ Review - Arrays",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2494684",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R5", "name" : "C++ Review - Classes",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2494685",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
        
        { "key" : "R6", "name" : "C++ Review - Pointers and memory",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2494633",
            "due" : "Feb 2",
            "end" : "Feb 9",
        },
      ],
      
      "exercises" :
      [
        {
          "key"   : "U02E", "name" : "C++ Review",
          "docs"  : "ASSIGNMENT_U02E_CPPReview.html",
          "350"   : "https://canvas.jccc.edu/courses/52475/assignments/1075454",
          "due"   : "Feb 2",
          "end"   : "Feb 7",
        },
        {
          "key"   : "C1", "name" : "Check-in (Week 2)",
          "350"   : "https://canvas.jccc.edu/courses/52475/modules/items/2462845",
          "due"   : "Jan 28",
          "end"   : "Jan 29",
        },
      ],
      
      "exams" :           
      [
        { "key" : "T1", "name" : "Units 2 - 4 Test ",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2608614",
            "due" : "Mar 2",
            "end" : "Mar 2",
        },
      ]
  },

  "Unit 3" : 
  {
      "link" : "unit03.html",
      "topic" : "Templates, Exception Handling, and STL Structures",

      "lectures" :        [
        { "name" : "Exception handling",
            "url" : "http://lectures.moosader.com/cs200/06-Exceptions.mp4"
        },
        { "name" : "Templates",
            "url" : "http://lectures.moosader.com/cs200/26-Templates.mp4"
        },
      ],
      "reading" :         [
        { "name" : "Chapter 12: Advanced Object Oriented Programming (12.2 Templates)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf"
        },
        { "name" : "Chapter 15: Exception handling with try/catch",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter15-exceptions.pdf"
        },
      ],
      
      "exercises" :
      [
        {
          "key"   : "U03E", "name" : "Templates, Exceptions, and STL Structures",
          "docs"  : "ASSIGNMENT_U03E_Templates_Exceptions_STL.html",
          "350"   : "https://canvas.jccc.edu/courses/52475/modules/items/2595325",
          "due"   : "Feb 9",
          "end"   : "Feb 11",
        },
      ],
      
      "quizzes" :         
      [
        { "key" : "U03Q1", "name" : "Templates",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2595338",
            "due" : "Feb 9",
            "end" : "Feb 16",
        },
        { "key" : "U03Q2", "name" : "Exception handling",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2595339",
            "due" : "Feb 9",
            "end" : "Feb 16",
        },
      ],
      
      "exams" :           
      [
        { "key" : "T1", "name" : "Units 2 - 4 Test ",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2608614",
            "due" : "Mar 2",
            "end" : "Mar 2",
        },
      ]
      
  },

  "Unit 4" : 
  {
      "link" : "unit04.html",
      "topic" : "Tests, Debugging, Navigating your IDE, and Project 1",
      
      "reading" :         [
        { "name" : "Chapter 19: Testing", "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter19-testing.pdf"
        },
      ],
      "lectures" :        [
        { "name" : "What is TDD? What is Test Driven Development? (Quick overview) - Development That Pays",
            "url" : "https://www.youtube.com/watch?v=H4Hf3pji7Fw"
        },
        { "name" : "How to DEBUG C++ in VISUAL STUDIO - The Cherno",
            "url" : "https://www.youtube.com/watch?v=0ebzPwixrJA"
        },
      ],
      "exercises" :       
      [
        { "key" : "U04E1", "name" : "Unit tests",
            "docs" : "ASSIGNMENT_U04E1_Unit_tests.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2601316",
            "due" : "Feb 16",
            "end" : "Feb 18",
        },
        { "key" : "U04E2", "name" : "Debugging tools and navigating your IDE",
            "docs" : "ASSIGNMENT_U04E2_Debugging_and_navigating.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2598317",
            "due" : "Feb 16",
            "end" : "Feb 18",
        },
      ],
      "projects" :        [
        { "key" : "P1", "name" : "Fooddood Project (v1)",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2602046",
            "due" : "Feb 28",
            "end" : "Feb 28",
        },
      ],
      
      "exams" :           
      [
        { "key" : "T1", "name" : "Units 2 - 4 Test ",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2608614",
            "due" : "Mar 2",
            "end" : "Mar 2",
        },
      ]
  },

  "Unit 5" : 
  {
      "link" : "unit05.html",
      "topic" : "Review: Pointers and Inheritance",

      "lectures" :        
      [
        { "name" : "Pointers",
            "url" : "http://lectures.moosader.com/cs200/16-Pointer.mp4"
        },
        { "name" : "Memory management",
            "url" : "http://lectures.moosader.com/cs200/17-Memory-Management.mp4"
        },
        { "name" : "Dynamic arrays",
            "url" : "http://lectures.moosader.com/cs200/18-Dynamic-Arrays.mp4"
        },
        { "name" : "Class inheritance",
            "url" : "http://lectures.moosader.com/cs200/15-Inheritance.mp4"
        },
      ],
      "reading" :         
      [
        { "name" : "Chapter 14: Pointers, memory management, and dynamic variables and arrays",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter14-pointers_and_memory.pdf"
        },
        { "name" : "Chapter 11: Intermediate Object Oriented Programming (11.6: Inheritance)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf"
        },
      ],
      "exercises" :       
      [
        { "key" : "U05E1", "name" : "Pointer review",
            "docs" : "ASSIGNMENT_U05E1_Pointer_review.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2608808",
            "due" : "Mar 2",
            "end" : "Mar 8",
        },
        { "key" : "U05E2", "name" : "Inheritance review",
            "docs" : "ASSIGNMENT_U05E2_InheritanceReview.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2610102",
            "due" : "Mar 2",
            "end" : "Mar 8",
        },
      ],
      "exams" :           
      [
        { "key" : "T1", "name" : "Units 2 - 4 Test ",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2608614",
            "due" : "Mar 2",
            "end" : "Mar 2",
        },
      ],
      "projects" :        [
        { "key" : "P1", "name" : "Fooddood Project (v2)",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2619774",
            "due" : "Mar 31",
            "end" : "Mar 31",
        },
      ],
  },

  "Unit 6" : 
  {
      "link" : "unit06.html",
      "topic" : "Interfaces and Polymorphism",

      "lectures" :        
      [
        { "name" : "Class inheritance",
            "url" : "http://lectures.moosader.com/cs200/15-Inheritance.mp4"
        },
        { "name" : "Polymorphism",
            "url" : "http://lectures.moosader.com/cs200/22-Polymorphism.mp4"
        },
      ],
      "reading" :         
      [
        { "name" : "Chapter 11: Intermediate Object Oriented Programming (11.6: Inheritance)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf"
        },
        { "name" : "Chapter 12: Advanced Object Oriented Programming (12.1: Polymorphism)",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter12-oop3.pdf"
        },
      ],
      "exercises" :       
      [
        { "key" : "U06E", "name" : "Polymorphism",
            "docs" : "ASSIGNMENT_U06E_Polymorphism.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2617096",
            "due" : "Mar 9",
            "end" : "Mar 16",
        },
      ],
      "projects" :        [
        { "key" : "P1", "name" : "Fooddood Project (v2)",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2619774",
            "due" : "Mar 31",
            "end" : "Mar 31",
        },
      ],
      
  },

  "Unit 7" : {
      "link" : "Projects.html",
      "topic" : "Project 2",

      "projects" :        [
        { "key" : "P1", "name" : "Fooddood Project (v2)",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2619774",
            "due" : "Mar 31",
            "end" : "Mar 31",
        },
      ],
  },

  "Unit 8" : {
      "link" : "unit08.html",
      "topic" : "Overloaded Operators, Copy Constructors, and Default Parameters",

      "lectures" :        [
        { "name" : "Copy constructors",
            "url" : "http://lectures.moosader.com/cs235/example-code/cs235-copy-constructor.mp4"
        },
        { "name" : "Overloaded operators",
            "url" : "http://lectures.moosader.com/cs200/20-Operator-Overloading.mp4"
        },
      ],
      
      "reading" :         [
        { "name" : "Chapter 10: Basic Object Oriented Programming ",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter10-oop1.pdf"
        },
        { "name" : "Chapter 11: Intermediate Object Oriented Programming",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf"
        },
      ],
      
      "exercises" :       
      [
        { "key" : "U08E", "name" : "Operator Overloading and Copy Constructors",
            "docs" : "ASSIGNMENT_U08E_OperatorOverloading.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2634641",
            "due" : "Mar 30",
            "end" : "Apr 6",
        },
      ],
      "tech-literacy" :
      [
        { "key" : "TL2", "name" : "C++ core guidelines",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2634646",
            "due" : "Mar 30",
            "end" : "Apr 6",
        },
      ],
  },

  "Unit 9" : {
      "link" : "unit09.html",
      "topic" : "Friends, Static Members, Functors, and Lambda Expressions",

      "reading" :         [
        { "name" : "Chapter 10: Basic Object Oriented Programming ",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter10-oop1.pdf"
        },
        { "name" : "Chapter 11: Intermediate Object Oriented Programming",
            "url" : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/standalone-chapter11-oop2.pdf"
        },
      ],
      
      "exercises" :       
      [
        { "key" : "U09E", "name" : "Friends, Static Members, and Lambda Expressions",
            "docs" : "ASSIGNMENT_U09E_Friends_Static_LambdaExpressions.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2639240",
            "due" : "Apr 6",
            "end" : "Apr 13",
        },
      ],
      "tech-literacy" :
      [
        { "key" : "TL3", "name" : "Code licenses",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2618712",
            "due" : "Apr 6",
            "end" : "Apr 13",
        },
      ],
  },

  "Unit 10" : {
      "link" : "unit10.html",
      "topic" : "Project 3",

      "projects" :        [
        { "key" : "P3", "name" : "Fooddood Project (v3)",
            "docs" : "Projects.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2644736",
            "due" : "Apr 30",
            "end" : "May 1",
        },
      ],
  },


  "Unit 11" : {
      "link" : "unit11.html",
      //"topic" : "Enums, Namespaces, and Linking third party libraries",
      "topic" : "Linking third party libraries",

      "exercises" :       
      [
        { "key" : "U11E1", "name" : "Linking third party libraries",
            "docs" : "ASSIGNMENT_U11E1_LinkingLibraries.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2651060",
            "due" : "Apr 27",
            "end" : "May 4",
        },
        { "key" : "U11E2", "name" : "A small graphical game",
            "docs" : "ASSIGNMENT_U11E2_GraphicalGame.html",
            "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2651061",
            "due" : "Apr 27",
            "end" : "May 4",
        },
      ],
      
      "exams" :           
      [
        { "key" : "T1", "name" : "Units 8 - 9 Test ",
            "350" : "",
            "due" : "Apr 30",
            "end" : "Apr 30",
        },
      ]
  },

  "Unit 12" : {
    "link" : "unit12.html",
    "topic" : "Student projects",
    
    "projects" :        
    [   
      { "key" : "PS",     "name" : "Student Project",
          "docs" : "StudentProject.html",
          "350" : "https://canvas.jccc.edu/courses/52475/modules/items/2649878",
          "due" : "May 2",
          "end" : "May 6",
      },
    ],
    
  },

  "TEMPLATE" : {
  },
};

} );
