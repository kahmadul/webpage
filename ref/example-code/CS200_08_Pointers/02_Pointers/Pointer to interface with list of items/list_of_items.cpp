#include <iostream>
#include <string>
using namespace std;


int main()
{
	const int CLASS_COUNT = 4;

	string undergradClasses[CLASS_COUNT] = { "none", "none", "none", "none" };
	string gradClasses[CLASS_COUNT] = { "none", "none", "none", "none" };

	string* ptrCurrentClass = nullptr;

	while (true)
	{
		cout << "Ungrad: " << endl;
		for (int i = 0; i < CLASS_COUNT; i++)
		{
			cout << i << ". \t " << undergradClasses[i] << endl;
		}
		cout << "Grad: " << endl;
		for (int i = 0; i < CLASS_COUNT; i++)
		{
			cout << i << ". \t " << gradClasses[i] << endl;
		}

		cout << endl << "Which class do you want to update?" << endl;
		
		cout << "Update 1. Undergrad, or 2. Grad" << endl;
		int level;
		cin >> level;

		if (level == 1)
		{
			for (int i = 0; i < CLASS_COUNT; i++)
			{
				cout << i << ". \t " << undergradClasses[i] << endl;
			}

			int choice;
			cout << ">> ";
			cin >> choice;

			ptrCurrentClass = &undergradClasses[choice];
		}
		else if (level == 2)
		{
			for (int i = 0; i < CLASS_COUNT; i++)
			{
				cout << i << ". \t " << gradClasses[i] << endl;
			}

			int choice;
			cout << ">> ";
			cin >> choice;

			ptrCurrentClass = &gradClasses[choice];
		}
		


		cout << endl;
		cout << "New class name: ";
		cin >> *ptrCurrentClass;

		cout << endl << "Updated" << endl;
	}




	return 0;
}
