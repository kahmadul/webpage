#include "RoomManager.hpp"

#include <fstream>
#include <iostream>
using namespace std;

Room * RoomManager::s_rooms;             // dynamic array
int RoomManager::s_totalRooms;
Room * RoomManager::s_ptrCurrentRoom;    // pointer to room

void RoomManager::Setup()
{
    s_rooms = nullptr;
    s_ptrCurrentRoom = nullptr;
    LoadMapData();
}

void RoomManager::Cleanup()
{
    DeallocateSpace();
}

void RoomManager::AllocateSpace( int size )
{
    DeallocateSpace();

    s_rooms = new Room[ size ];
    s_totalRooms = size;
}

void RoomManager::DeallocateSpace()
{
    if ( s_rooms != nullptr )
    {
        delete [] s_rooms;
        s_totalRooms = 0;
        s_rooms = nullptr;
    }
}

int RoomManager::GetIndexOfRoomWithName( string name )
{
    for ( int i = 0; i < s_totalRooms; i++ )
    {
        if ( s_rooms[i].m_name == name )
        {
            return i;
        }
    }
    return -1;
}

void RoomManager::LoadMapData()
{
    ifstream input( "data/rooms.txt" );
    string buffer;
    int totalRooms;
    int roomCounter = 0;

    input >> buffer;    // TOTAL_ROOMS
    input >> totalRooms;

    AllocateSpace( totalRooms );

    while ( getline( input, buffer ) )
    {
        if ( buffer == "NAME" )
        {
            getline( input, s_rooms[ roomCounter ].m_name );
        }
        else if ( buffer == "DESCRIPTION" )
        {
            getline( input, s_rooms[ roomCounter ].m_description );
        }
        else if ( buffer == "ROOM_END" )
        {
            roomCounter++;
        }
        else if ( buffer == "NEIGHBORS" )
        {
            string roomName1, roomName2;
            int roomIndex1, roomIndex2;
            string directionString;

            // Room name
            getline( input, roomName1 );
            getline( input, directionString );
            getline( input, roomName2 );

            roomIndex1 = GetIndexOfRoomWithName( roomName1 );
            roomIndex2 = GetIndexOfRoomWithName( roomName2 );

            if ( directionString == "WEST" )
            {
                s_rooms[ roomIndex1 ].m_ptrNeighborWest = &s_rooms[ roomIndex2 ];
                s_rooms[ roomIndex2 ].m_ptrNeighborEast = &s_rooms[ roomIndex1 ];
            }
            else if ( directionString == "EAST" )
            {
                s_rooms[ roomIndex1 ].m_ptrNeighborEast = &s_rooms[ roomIndex2 ];
                s_rooms[ roomIndex2 ].m_ptrNeighborWest = &s_rooms[ roomIndex1 ];
            }
            else if ( directionString == "NORTH" )
            {
                s_rooms[ roomIndex1 ].m_ptrNeighborNorth = &s_rooms[ roomIndex2 ];
                s_rooms[ roomIndex2 ].m_ptrNeighborSouth = &s_rooms[ roomIndex1 ];
            }
            else if ( directionString == "SOUTH" )
            {
                s_rooms[ roomIndex1 ].m_ptrNeighborSouth = &s_rooms[ roomIndex2 ];
                s_rooms[ roomIndex2 ].m_ptrNeighborNorth = &s_rooms[ roomIndex1 ];
            }
        }
    }

    // Set the first room to room #0
    s_ptrCurrentRoom = &s_rooms[0];
}

void RoomManager::TryToMove( string command )
{
    cout << endl;
    if ( command == "west" || command == "w" )
    {
        if ( s_ptrCurrentRoom->m_ptrNeighborWest == nullptr )
        {
            cout << "Cannot move west!" << endl;
        }
        else
        {
            cout << "You moved west." << endl;
            s_ptrCurrentRoom = s_ptrCurrentRoom->m_ptrNeighborWest;
        }
    }
    else if ( command == "east" || command == "e" )
    {
        if ( s_ptrCurrentRoom->m_ptrNeighborEast == nullptr )
        {
            cout << "Cannot move east!" << endl;
        }
        else
        {
            cout << "You moved east." << endl;
            s_ptrCurrentRoom = s_ptrCurrentRoom->m_ptrNeighborEast;
        }
    }
    else if ( command == "north" || command == "n" )
    {
        if ( s_ptrCurrentRoom->m_ptrNeighborNorth == nullptr )
        {
            cout << "Cannot move north!" << endl;
        }
        else
        {
            cout << "You moved north." << endl;
            s_ptrCurrentRoom = s_ptrCurrentRoom->m_ptrNeighborNorth;
        }
    }
    else if ( command == "south" || command == "s" )
    {
        if ( s_ptrCurrentRoom->m_ptrNeighborSouth == nullptr )
        {
            cout << "Cannot move south!" << endl;
        }
        else
        {
            cout << "You moved south." << endl;
            s_ptrCurrentRoom = s_ptrCurrentRoom->m_ptrNeighborSouth;
        }
    }
    else
    {
        cout << "Unknown command!" << endl;
    }
    cout << endl;
}

void RoomManager::DisplayCurrentRoom()
{
    s_ptrCurrentRoom->Display();
}
