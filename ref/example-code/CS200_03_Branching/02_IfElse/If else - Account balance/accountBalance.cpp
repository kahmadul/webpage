#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    float price;    // The price of some item
    float balance;  // The user's account balance

    cout << "What is your account balance: ";
    cin >> balance;

    cout << "How much does the item cost? ";
    cin >> price;

    // WAY 1:
    if ( price > balance )  // User doesn't have enough money
    {
        cout << "Card declined. :(" << endl;
    }
    else                    // User does have enough money
    {
        cout << "Card accepted! :)" << endl;
        balance = balance - price;
    }

    // WAY 2:
    if ( price <= balance ) // User does have enough money
    {
        cout << "Card accepted! :)" << endl;
        balance = balance - price;
    }
    else                    // User doesn't have enough money.
    {
        cout << "Card declined. :(" << endl;
    }



    cout << "Your balance is now: $" << balance << endl;

    return 0;
}

