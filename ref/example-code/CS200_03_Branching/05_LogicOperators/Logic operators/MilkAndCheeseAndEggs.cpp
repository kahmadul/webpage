#include <iostream>
using namespace std;

int main()
{
    // AND && and OR ||

    bool milk, cheese, eggs;

    cout << "Do you have milk? (0 = false, 1 = true): ";
    cin >> milk;

    cout << "Do you have cheese? (0 = false, 1 = true): ";
    cin >> cheese;

    cout << "Do you have eggs? (0 = false, 1 = true): ";
    cin >> eggs;

    cout << endl;

    // Using AND
    if ( cheese && eggs )
    {
        cout << "Make an cheese omelette" << endl;
    }
    else if ( milk && cheese )
    {
        cout << "Make macaroni" << endl;
    }
    else
    {
        cout << "Go to the grocery store" << endl;
    }

    // Using OR
    if ( milk || cheese )
    {
        cout << "You have dairy in the fridge" << endl;
    }
    else
    {
        // Opposite of ( milk || cheese ) == !( milk || cheese )
        //                                == !milk && !cheese
        cout << "You have no dairy in the fridge" << endl;
    }

    return 0;
}
