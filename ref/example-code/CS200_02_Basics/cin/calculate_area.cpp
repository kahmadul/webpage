// Library: pre-written code
#include <iostream>     // input / output streams
using namespace std;    // Using C++'s Standard Library

int main()
{
    // Variable declarations:
    int width;
    int length;
    int area;

    // Assignment operator: =
    // Assignment statements:
    width = 20;
    length = 10;

    // cin (Console Input) command:
    // >> = input stream operator
    cout << "Please enter width: ";
    cin >> width;

    cout << "Please enter length: ";
    cin >> length;

    cout << length;

    cout << "Now I'll calculate the area!" << endl << endl;

    area = width * length;  // * = multiplication

    // cout (Console Output) command:
    // << = output stream operator
    // endl = end line
    //  \t = tab
    cout << "Width is \t" << width << endl;
    cout << "Length is \t" << length << endl;
    cout << "Area is \t" << area << endl;

    cout << endl << endl;

    // Using tabs for a simple table:
    cout << "A \t B \t C" << endl;
    cout << "1 \t 2 \t 3" << endl;
    cout << "4 \t 5 \t 6" << endl;

    return 0;
}
