#include <iostream>     // console input and output (cin, cout)
using namespace std;

int main()
{
    cout << "----------------------" << endl;
    cout << "-     STORY TIME     -" << endl;
    cout << "----------------------" << endl;
    cout << endl;
    cout << "Once upon a time there was a cat named Kabe." << endl;
    cout << "Kabe was a lazy cat, and avoided the other cats." << endl;
    cout << endl;
    cout << "One day one of the other cats was sleeping in Kabe's spot." << endl;
    cout << "Kabe decided to sleep in a kitchen cabinet instead." << endl;
    cout << endl;
    cout << "The end." << endl;


    return 0;
}
