#include <iostream>
#include <string>
using namespace std;

#include "Employee.hpp"

int main()
{
    Employee boss;
    boss.Setup( "Bossman", "The Boss", nullptr );

    Employee underling1( "Underling1", "Employee", &boss );

    Employee underling2( underling1 );
    underling2.SetName( "Underling2" );

    boss.Display();
    underling1.Display();
    underling2.Display();

    return 0;
}
