#include <iostream>
#include <string>
using namespace std;

int main()
{
    string text;
    int zCount = 0;

    cout << "Enter some text: ";
    getline( cin, text );

    // Iterate from i=0 to the size of the string (not-inclusive)
    for ( unsigned int i = 0; i < text.size(); i++ )
    {
        // If this letter is a lower-case z or upper-case Z
        if ( text[i] == 'z' || text[i] == 'Z' )
        {
            // Add one to z count
            zCount++;
        }
    }

    // Display the result
    cout << "There were " << zCount
        << " z(s) in the string!" << endl;
    
    return 0;
}
