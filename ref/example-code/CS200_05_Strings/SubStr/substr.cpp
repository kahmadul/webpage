#include <iostream>
#include <string>
using namespace std;

int main()
{
    string text = "Name: Bob";
    
    int start = 6;
    int length = 3;
    
    string name = text.substr( start, length );
    
    cout << "Extracted \"" << name << "\"." << endl;
    
    return 0;
}
