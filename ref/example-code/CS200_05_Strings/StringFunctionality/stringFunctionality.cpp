#include <iostream>
#include <string>
using namespace std;

int main()
{
    bool done = false;
    while ( !done )
    {
        cout << "------------------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "------------------------------" << endl;
        cout << "1. size()" << endl;
        cout << "2. operator[]" << endl;
        cout << "3. find()" << endl;
        cout << "4. substr()" << endl;
        cout << "5. compare()" << endl;
        cout << "6. operator+" << endl;
        cout << "7. insert()" << endl;
        cout << "8. erase()" << endl;
        cout << "9. replace()" << endl;
        cout << "0. EXIT" << endl;
        cout << endl << ">> ";
        int choice;
        cin >> choice;

        switch( choice )
        {
            case 1: {       // size()
            // https://www.cplusplus.com/reference/string/string/size/

            cout << "Please enter a string: ";
            string text;

            //cin >> text;          // stops at spaces
            cin.ignore();
            getline( cin, text );   // get a whole line of text (with spaces)

            cout << "There are " << text.size() << " characters in the string." << endl;

            } break;

            case 2: {       // operator[]
            // https://www.cplusplus.com/reference/string/string/operator[]/

            cout << "Please enter a string: ";
            string text;
            cin.ignore();
            getline( cin, text );

            cout << "The first letter is " << text[0] << endl;
            cout << "The second letter is " << text[1] << endl;
            cout << "The third letter is " << text[2] << endl;

            if ( text[0] == 'H' )
            {
                cout << "Are you typing Hello?" << endl;
            }

            } break;

            case 3: {       // find()
            // https://www.cplusplus.com/reference/string/string/find/

            string text = "Hello, world!";

            cout << "What do you want to search for? ";
            string findme;
            cin.ignore();
            getline( cin, findme );

            int findPosition = text.find( findme );

            if ( findPosition == string::npos )
            {
                cout << "String not found" << endl;
            }
            else
            {
                cout << "String found at position " << findPosition << endl;
            }

            } break;

            case 4: {       // substr()
            // https://www.cplusplus.com/reference/string/string/substr/

            string text = "Hello, world!";

            cout << "Enter a starting position: ";
            int start;
            cin >> start;

            cout << "Enter a length: ";
            int length;
            cin >> length;

            string newString = text.substr( start, length );

            cout << "The new string is: \"" << newString << "\"" << endl;

            } break;

            case 5: {       // compare()
            // https://www.cplusplus.com/reference/string/string/compare/

            string string1, string2;
            cout << "Enter string 1: ";
            cin.ignore();
            getline( cin, string1 );

            cout << "Enter string 2: ";
            getline( cin, string2 );

            int result = string1.compare( string2 );

            if ( result == 0 )
            {
                cout << "The strings are the same" << endl;
            }
            else if ( result < 0 )
            {
                cout << string1 << " < " << string2 << endl;
            }
            else if ( result > 0 )
            {
                cout << string1 << " > " << string2 << endl;
            }

            } break;

            case 6: {       // operator+
            // https://www.cplusplus.com/reference/string/string/operator+=/

            cout << "EXAMPLE 1" << endl;
            string string1, string2;
            cout << "Enter string 1: ";
            cin.ignore();
            getline( cin, string1 );

            cout << "Enter string 2: ";
            getline( cin, string2 );

            string string3 = string1 + string2;
            cout << "Result: " << string3 << endl;



            cout << endl << "EXAMPLE 2" << endl;
            string text = "Hello";
            cout << "Original text " << text << endl;

            cout << "What do you want to append to the text: ";
            string appendMe;
            getline( cin, appendMe );

            text += appendMe;
            cout << "Text is now " << text << endl;

            } break;

            case 7: {       // insert()
            // https://www.cplusplus.com/reference/string/string/insert/

            string text = "HelloWorld";
            cout << "Original text: \"" << text << "\"" << endl;

            cout << "Enter a string to insert: ";
            string insertMe;
            cin.ignore();
            getline( cin, insertMe );

            cout << "Enter a position to insert at: ";
            int insertLocation;
            cin >> insertLocation;

            text = text.insert( insertLocation, insertMe );

            cout << "New text: \"" << text << "\"" << endl;

            } break;

            case 8: {       // erase()
            // https://www.cplusplus.com/reference/string/string/erase/

            string text = "HelloWorld";
            cout << "Original text: \"" << text << "\"" << endl;

            cout << "Enter a position to begin erasing at: ";
            int start;
            cin >> start;

            cout << "Enter a length of chars to remove: ";
            int length;
            cin >> length;

            text = text.erase( start, length );

            cout << "New text: \"" << text << "\"" << endl;

            } break;

            case 9: {       // replace()
            // https://www.cplusplus.com/reference/string/string/replace/



            string text = "HelloWorld";
            cout << "Original text: \"" << text << "\"" << endl;

            cout << "Enter a position to begin erasing at: ";
            int start;
            cin >> start;

            cout << "Enter a length of chars to remove: ";
            int length;
            cin >> length;

            cout << "Enter a string to insert at that position: ";
            string addMe;
            cin.ignore();
            getline( cin, addMe );

            text = text.replace( start, length, addMe );

            cout << "New text: \"" << text << "\"" << endl;



            } break;

            case 0: {
            done = true;
            } break;
        }
    }
}
