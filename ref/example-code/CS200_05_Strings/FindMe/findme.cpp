#include <iostream>
#include <string>
using namespace std;

int main()
{
    string str = "this was written during the 2021 winter storm make it stop please.";

    string findMe = "winter";

    size_t position = str.find( findMe );

    cout << "The text " << findMe 
        << " was found at position " << position << endl;
    
    return 0;
}
