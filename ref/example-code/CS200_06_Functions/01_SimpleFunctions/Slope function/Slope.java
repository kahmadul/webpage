import java.util.Scanner;

public class Slope
{
    static Scanner input = new Scanner( System.in );
        
    public static double GetSlope( double x1, double y1, double x2, double y2 )
    {
        return ( y2 - y1 ) / ( x2 - x1 );
    }

    public static boolean RunAgain()
    {
        System.out.print( "Run again? (y/n): " );
        String choice = input.next();

        if ( choice.equals( "n" ) )
        {
            return false;
        }
        return true;
    }
    
    public static void main( String args[] )
    {
        double x1, y1, x2, y2;
        
        do
        {
            System.out.print( "Enter x1, y1: " );
            x1 = input.nextDouble();
            y1 = input.nextDouble();
            
            System.out.print( "Enter x2, y2: " );
            x2 = input.nextDouble();
            y2 = input.nextDouble();

            double slope = GetSlope( x1, y1, x2, y2 );
            System.out.println( "\n\t Slope: " + slope );
            System.out.println();
        } while ( RunAgain() );
    }
}
