// CS 235
$( document ).ready( function() {

    headerInfo = {
        "course"        : "CS 235: Object Oriented Programming with C++",
        "dates"         : "2021-08-23 to 2021-12-13",
        "semester"      : "Fall 2021",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs235/",
        "textbook1"     : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_235",
        "syllabus"      : "syllabus.html",
        "exampleCode"   : "https://gitlab.com/rachels-courses/cs235-object-oriented-programming-with-cpp/-/tree/main/example-code"
    };
    
    sectionInfo = [
        {
            "semester"      : "Fall 2021",
            "crn"           : "81839",
            "section"       : "350",
            "method"        : "Online only",
            "times"         : "Online only",
            "background"    : "bg1"
        },
        {
            "semester"      : "Fall 2021",
            "crn"           : "82836",
            "section"       : "351",
            "method"        : "Online only",
            "times"         : "Online only",
            "background"    : "bg2"
        },
    ];
    
} );
