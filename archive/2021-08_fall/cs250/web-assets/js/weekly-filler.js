$( document ).ready( function()
{    
    for ( var i = 0; i < scheduleInfo.length; i++ )
    {
        var date = new Date( firstMondayOfClass );
        if ( i > 0 )
        {
            date.setDate( firstMondayOfClass.getDate() + 7 * i );
        }
        
        var dayString = "Week of " + freakingMonths[ date.getMonth() ] + " " + date.getDate().toString();
        
        var tableHtml   = "<tr class='d-flex'>"
        tableHtml       += "<td class='col-1 week'> " + scheduleInfo[i]["week"] + "</td>";     // Week
        tableHtml       += "<td class='col-2 date'> " + dayString + " </td>";    // Date
        if ( scheduleInfo[i]["link"] == "" )
        {
            tableHtml += "<td class='col-6 link'> " + scheduleInfo[i]["topic"] + "</td>";
        }
        else
        {
            tableHtml       += "<td class='col-6 link'> <a href='" + scheduleInfo[i]["link"] + "'>" + scheduleInfo[i]["unit"] + ": " + scheduleInfo[i]["topic"] + "</a> </td>";
        }
        tableHtml       += "<td class='col-3 notes'> " + scheduleInfo[i]["notes"] + " </td>";
        tableHtml       += "</tr>\n";
        $( "#weekly-schedule" ).append( tableHtml );
    }
} 
);
