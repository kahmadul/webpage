$( document ).ready( function()
{
    var index = parseInt( $( "#index-data" ).val() );
    console.log( "Unit index:", index );
    
    var weeklyInfo = scheduleInfo[ index ];
    console.log( weeklyInfo );
    
    PopulateList( "due", "due" );
    PopulateList( "lectures", "lectures" );
    PopulateList( "exercises", "exercises" );
    PopulateList( "quizzes", "quizzes" );
    PopulateList( "projects", "projects" );
    PopulateList( "exams", "exams" );
    
    function PopulateList( boxClass, dataClass )
    {
        if ( weeklyInfo[dataClass] === undefined )
        {
            // Make empty boxes invisible.
            $( ".card-" + boxClass ).addClass( "invisible" );
            return;
        }
        
        var links = "";
        for ( var i = 0; i < weeklyInfo[dataClass].length; i++ )
        {
            var info = weeklyInfo[dataClass][i];
            links += "<li>" + info["item"] + "<ul>";
            for ( var j = 0; j < info["array"].length; j++ )
            {
                var assignmentItem = info["array"][j];
                if ( assignmentItem[ "type" ] == "video" )
                {
                    links += "<li><a href='" + assignmentItem["url"] + "'>" + assignmentItem["name"] + "</a></li>";                    
                }
                else if ( assignmentItem[ "type" ] == "code" )
                {
                    links += "<li><a href='" + assignmentItem["url"] + "'>" + assignmentItem["name"] + "</a></li>";
                }
                else if ( assignmentItem[ "type" ] == "reading" )
                {
                    links += "<li><a href=\"" + assignmentItem["where"] + "\">" + assignmentItem["name"] + "</a></li>";
                }
                else if ( assignmentItem[ "type" ] == "Canvas quiz" )
                {
                    links += assignmentItem["where"];
                }
                else if ( assignmentItem[ "type" ] == "Canvas exam" )
                {
                    links += assignmentItem["where"];
                }
                else if ( assignmentItem[ "type" ] == "exercise" )
                {
                    console.log( assignmentItem );
                    var exercise = "";
                    if ( assignmentItem["docs"] !== undefined && assignmentItem["docs"] != "#" )
                    {
                        exercise += "<li><a href='" + assignmentItem["docs"] + "'>📋 Documentation</a></li>";
                    }
                    if ( assignmentItem["starter"] !== undefined && assignmentItem["starter"] != "#" )
                    {
                        exercise += "<li><a href='" + assignmentItem["starter"] + "'>💾 Starter code</a></li>";
                    }
                    if ( assignmentItem["video"] !== undefined&& assignmentItem["video"] != "#" )
                    {
                        exercise += "<li><a href='" + assignmentItem["video"] + "'>🎥 Walkthru video</a></li>";
                    }
                    if ( assignmentItem["canvas"] !== undefined && assignmentItem["canvas"] == "yes" )
                    {
                        exercise += "<li><img src='web-assets/graphics/canvas-icon.png'> Available on Canvas</li>";
                    }
                    links += exercise;
                }
                else if ( assignmentItem[ "type" ] == "project" )
                {
                    var project = "";
                    if ( assignmentItem["docs"] !== undefined && assignmentItem["docs"] != "#" )
                    {
                        project += "<li><a href='" + assignmentItem["docs"] + "'>📋 Documentation</a></li>";
                    }
                    if ( assignmentItem["starter"] !== undefined && assignmentItem["starter"] != "#" )
                    {
                        project += "<li><a href='" + assignmentItem["starter"] + "'>💾 Starter code</a></li>";
                    }
                    if ( assignmentItem["video"] !== undefined && assignmentItem["video"] != "#" )
                    {
                        project += "<li><a href='" + assignmentItem["video"] + "'>🎥 Video</a></li>";
                    }
                    links += project;
                }
                else if ( assignmentItem[ "type" ] == "due" )
                {
                    var due = "<li>" + assignmentItem["name"] + "</li>";
                    links += due;
                }
                else if ( assignmentItem[ "type" ] == "raw" )
                {
                    var due = "<li>" + assignmentItem["name"] + "</li>";
                    links += due;
                }
            }
            links += "</ul></li>";
        }
        $( "ul.todo-" + boxClass ).append( links );
        
        if ( links == "" )
        {
            // Make empty boxes invisible.
            $( ".card-" + boxClass ).addClass( "invisible" );
        }
    }
    
    $( ".click-to-expand" ).click( function() {
        //$( this ).slideUp( "fast", function() {} );
        $( this ).next( ".hint-contents" ).slideDown( "fast", function() {} );
    } );
    
    /*
    var index = $( "#index-data" ).val();
    var entry = scheduleInfo[index];
    
    console.log( index, entry );
    
    $( "#unit" ).html( entry["unit"] );
    
    var date = new Date( firstMondayOfClass );
    var i = index - 1;
    if ( i > 0 )
    {
        date.setDate( firstMondayOfClass.getDate() + 7 * i );
    }
    var dayString = "Week of " + freakingMonths[ date.getMonth() ] + " " + date.getDate().toString();
    $( "#week" ).html( dayString );
    
    $( "#topics" ).html( entry["topic"] );
    */
}
);
