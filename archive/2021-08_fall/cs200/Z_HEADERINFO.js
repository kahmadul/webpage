// CS 200
$( document ).ready( function() {

    headerInfo = {
        "courseCode"    : "CS 200",
        "course"        : "CS 200: Concepts of Programming with C++",
        "dates"         : "2021-08-23 to 2021-12-13",
        "semester"      : "Fall 2021",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs200/",
        "textbook1"     : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_200",
        "syllabus"      : "syllabus.html",
        "exampleCode"   : "https://gitlab.com/rachels-courses/cs200-concepts-of-programming-with-cpp/-/tree/main/example-code"
    };
    
    sectionInfo = [
        {
            "semester"      : "Fall 2021",
            "crn"           : "81827",
            "section"       : "350",
            "method"        : "Online only",
            "times"         : "Online only",
            "background"    : "bg1"
        },
        {
            "semester"      : "Fall 2021",
            "crn"           : "81830",
            "section"       : "378",
            "method"        : "Online hybrid",
            "times"         : "Tues, 6:00 - 8:50 pm",
            "background"    : "bg2"
        }
    ];
    
} );
