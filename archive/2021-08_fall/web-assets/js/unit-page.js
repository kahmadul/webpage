$( document ).ready( function()
{
    // Populating the content list on the unit page
    var unitKey = $( "#unit-key" ).val();
    console.log( "Unit:", unitKey );
    var weeklyInfo = unitInfo[unitKey];
    console.log( weeklyInfo );

    // Populate tab / header ( <span class="insert-unit"></span> - <span class="insert-topics"> )
    $( ".insert-unit" ).html( unitKey );
    $( ".insert-topics" ).html( weeklyInfo["topic"] );

    // Sections: .reminders, .lectures, .reading, .archives, .exercises, .quizzes, .tech-literacy
    var types = [ "reminders", "lectures", "reading", "archives", "exercises", "quizzes", "projects", "tech-literacy", "exams" ];
    types.forEach( function( type ) {
        PopulateContent( type );
    } );

    function PopulateContent( key )
    {
        var sectionName = "section." + key;
        if ( !ThisExists( weeklyInfo ) )
        {
            console.log( "Something weird happened. weeklyInfo is undefined for some reason." );
            return;
        }
        else if ( !ThisExists( weeklyInfo[key] ) )
        {
            // Make this section invisible
            $( sectionName ).addClass( "invisible" );
            return;
        }

        var listItems = "";
        // reminders, lectures, archives, and reading are all lists.
        // exercises, quizzes, and tech literacy are tables.

        weeklyInfo[key].forEach( function( item ) {
            if ( key == "reminders" )
            {
                listItems += "<li>" + item + "</li>";
            }
            else if ( key == "lectures" || key == "reading" || key == "archives" )
            {
                listItems += "<li><a href='" + item["url"] + "'>" + item["name"] + "</a></li>";
            }
            else
            {
                var additionalClass = "";
                if      ( ThisExists( item["docs"] ) )  { additionalClass = "docs"; }
                else                                    { additionalClass = "nodocs"; }

                listItems += "<tr>";
                listItems += "<th class='key " + additionalClass + "'>" + "[" + item["key"] + "] " + "</th>";
                listItems += "<td class='name " + additionalClass + "'>";
                listItems += item["name"];
                if ( ThisExists( item["due"] ) )
                {
                    listItems += "<br><span class='due'>Due: " + item["due"] + "</span>";
                }
                if ( ThisExists( item["end"] ) )
                {
                    listItems += "<br><span class='end'>End: " + item["end"] + "</span>";
                }
                listItems += "</td>";

                if ( ThisExists( item["docs"] ) )
                {
                    listItems += "<td class='documents " + additionalClass + "'><a href='" + item["docs"] + "'>📋 Docs</a></td>";
                }
                else { listItems += "<td class='documents " + additionalClass + "'>&nbsp;</td>"; }
                if ( ThisExists( item["canvas"] ) )
                {
                    listItems += "<td class='canvas " + additionalClass + "'><a href='" + item["canvas"] + "'><img src='../web-assets/graphics/canvas-icon.png'> Canvas</a></td>";
                }
                else
                {
                    listItems += "<td class='canvas " + additionalClass + "'>";
                    var sectionLinks = "";
                    for ( var i = 0; i < sectionNumbers.length; i++ )
                    {
                      var sec = sectionNumbers[i];
                      if ( ThisExists( item[ sec ] ) )
                      {
                        if ( sectionLinks != "" ) { sectionLinks += "<br>"; }
                        sectionLinks += "<a href='" + item[sec] + "'><img src='../web-assets/graphics/canvas-icon.png'> Section " + sec + "</a>";
                      }
                    }
                    listItems += sectionLinks;
                    listItems += "</td>";
                }

                listItems += "</tr>";
            }
        } );

        $( sectionName + " .insert-here" ).append( listItems );
    }

}
);
