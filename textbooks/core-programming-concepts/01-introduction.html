<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Introduction - Core Programming Concepts - Chapter 1 </title>
  <!-- <title> Introduction - Intro to Data Structures </title> -->
  <!-- <title> Introduction - Discrete Math </title> -->
  
  <meta name="description" content="">
  <meta name="author" content="Rachel Singh">
  <link rel="icon" type="image/png" href="../../web-assets/favicon-cs200book.png">

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

  <!-- bootstrap -->
  <link rel="stylesheet" href="../../web-assets/bootstrap-dark/css/bootstrap-dark.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- highlight.js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/tomorrow-night-blue.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

  <!-- rachel's stuff -->
  <link rel="stylesheet" href="../../web-assets/style/2020_10.css">
  <link rel="stylesheet" href="../../web-assets/style/2020_10_mobile.css">
  <script src="../../web-assets/js/header-bar.js"></script>
  <script src="../../web-assets/js/utilities.js"></script>
</head>
<body>
    <div id="data" style="display: none;">
        <input type="text" id="index-data" value="4">
        <input type="text" id="prev_page" value="blah1.html">
        <input type="text" id="prev_page_name" value="Chapter X">
        <input type="text" id="next_page" value="02-writing-programs.html">
        <input type="text" id="next_page_name" value="Chapter 2: Writing programs">
    </div> <!-- data -->

    <div class="container-fluid textbook exercise">
        <div id="course-links"></div> <!-- JS inserts links -->        
        
        <div class="title row">
          <div class="col-md-8 chapter">
            <h1 id="class-info"> Introduction </h1>
          </div>
          <div class="col-md-4 book">
            Core Programming Concepts
          </div>
        </div>
        
        <a name="top">&nbsp;</a>
        
        <hr>
        
        <nav class="row booknav">
          <div class="col-md-4 prev"><!--<a href="" class="insert-prev_page">&lt;&lt; <span class="insert-prev_page_name"></span></a>--></div>
          <div class="col-md-4 main"><a href="../index.html">Back to Index</a></div>
          <div class="col-md-4 next"><a href="02-writing-programs.html" class="insert-next_page"><span class="insert-next_page_name"></span> &gt;&gt;</a></div>
        </nav>
        <hr>
        
        <!-- === BEGIN BOOK CONTENTS === -->
        <div class="row"><div class="col-md-12">
          
            <h2>Welcome!</h2>
          
            <div class="row">
              <div class="col-md-9">
                <p>
                  It feels weird to start a collection of notes (or a "textbook")
                  without some sort of welcome, though at the same time I know that
                  people are probably <em>not</em>
                  <span class="footnote" title="Unless I put some cute art and interesting footnotes, maybe.">going to read the introduction.</span>
                  <br><br>
                  I think that I will welcome you to my notes by addressing anxiety.
                </p>
              </div>
              <div class="col-md-3 fit-image">
                <img src="images/writing.png" title="A drawing of Rachel typing on a laptop, thinking 'uhhh...'">
              </div>
            </div>
            
            <h3>Belonging</h3>
            
            <p>
              Unfortunately there is a lot of bias in STEM fields and over
              decades there has been a narrative that computer science is <em>for</em>
              a certain type of person - antisocial, nerdy, people who started coding
              when they were 10 years old.
              <br><br>
              Because of this, a lot of people who don't fit this description can
              be hesitant to get into computers or programming because they don't
              see people like themselves in media portrayals. Or perhaps previous
              professors or peers have acted like you're not a <em>real programmer</em>
              if you didn't start programming as a child.
            </p>
            
            <p class="center">
              <strong>If you want to learn about coding, then you belong here. There are no prerequisites.</strong>
            </p>
            
            <p>
              I will say from my own experience, I know developers who fell in
              love with programming by accident as an adult after having to take
              a computer class for a <em>different degree</em>. I know developers who
              are into all sorts of sports, or into photography, or into fashion.
              There is no specific "type" of programmer. You can be any religion,
              any gender, any color, from any country, and be a programmer.
            </p>
            
            <p class="center">
              <img src="images/gatekeepingbaby.png" title="A drawing of a baby holding a floppy disk and a mouse, saying I can't even walk yet but I know how to code, unlike YOU posers!">
              <br><sub>Don't be like Gatekeeper Baby. You can begin coding at any age!</sub>
            </p>
            
            <h3>Challenge</h3>
            
            <div class="row">
              <div class="col-md-9">
                <p>
                  Programming can be hard sometimes. There are many aspects of learning
                  to write software (or websites, apps, games, etc.) and you <strong>will</strong>
                  get better at it with practice and with time. But I completely understand
                  the feeling of hitting your head against a wall wondering <em>why won't this work?!</em>
                  and even wondering <em>am I cut out for this?!</em> - Yes, you are.
                </p>
                
                <p>
                  I will tell you right now, I <em>have</em> cried over programming assignments,
                  over work, over software. I have taken my laptop with me to a family
                  holiday celebration because I <em>couldn't figure out this program</em>
                  and I <em>had to get it done!!</em>
                </p>
                <p>
                  All developers struggle. Software is a really unique field. It's very
                  intangible, and there are lots of programming languages, and all sorts
                  of tools, and various techniques. Nobody knows everything, and there's
                  always more to learn.
                </p>
                
                <p>
                  Just because something is <em>hard</em> doesn't mean that it's <em>impossible</em>.
                </p>
              </div>
              <div class="col-md-3 fit-image">
                <img src="images/justwork.png" title="A drawing of Rachel at a laptop crying, saying 'Why don't you just work?!'">
              </div>
            </div>
            
            <p>
              It's completely natural to hit roadblocks. To have to step away from
              your program and come back to it later with a clear head. It's natural
              to be confused. It's natural to <em>not know</em>.
              <br><br>              
              But some skills you will learn to make this process smoother are
              how to plan out your programs, test and verify your programs,
              how to phrase what you don't know as a question, how to ask for help.
              These are all skills that you will build up over time.
              Even if it feels like you're not making progress, I promise that you are,
              and hopefully at the end of our class you can look back to the start of it
              and realize how much you've learned and grown.
            </p>
            
            <h3>Learning</h3>
            
            <div class="row">
              <div class="col-md-9">
                <p>
                  First and foremost, I am here to help you <strong>learn</strong>.
                  <br><br>              
                  My teaching style is influenced on all my experiences throughout
                  my learning career, my software engineer career, and my teaching career.
                  <br><br>              
                  I have personally met teachers who have tried to <em>scare me away</em>
                  from computers, I've had teachers who really encouraged me, 
                  I've had teachers who barely cared, I've had teachers who made
                  class really fun and engaging.
                  <br><br>              
                  I've worked professionally in software and web development, and
                  independently making apps and video games. I know what it's like
                  to apply for jobs and work with teams of people and experience
                  a software's development throughout its lifecycle.
                  <br><br>              
                  And as a teacher I'm always trying to improve my classes -
                  making the learning resources easily available and accessible,
                  making assignments help build up your knowledge of the topics,
                  trying to give feedback to help you design and write good programs.
                  <br><br>              
                  As a teacher, I am not here to trick you with silly questions
                  or decide whether you're a <em>real programmer</em> or not;
                  I am here to help guide you to learn about programming,
                  learn about design, learn about testing, and learn how to teach yourself.
                </p>
              </div>
              <div class="col-md-3 fit-image">
                <img src="images/buildingblocks.png" title="A drawing of Rachel next to some building blocks stacked tall, saying 'Like this!'">
              </div>
            </div>
            
          <hr>
          <h2>Roadmap</h2>
          <p class="center"><img src="images/roadmap.png" title="A drawing of a roadmap. You are here is starred, and the path runs past 'Learning the language', then 'Problem solving and design', then 'Testing and validation', then 'Good development practice', and so on." style="width:50%;"></p>
          
          <p>
            When you're just starting out, it can be hard to know what all
            you're going to be learning about. I have certainly read course
            descriptions and just thought to myself "I have no idea what
            any of that meant, but it's required for my degree, so I guess
            I'm taking it!"
            <br><br>
            Here's kind of my mental map of how the courses I teach work:
          </p>
          
          <ul>
            <li><strong>CS 200: Concepts of Programming with C++</strong><br> You're learning the language. Think of it like actually
                    learning a human language; I'm teaching you words and
                    the grammar, and at first you're just parroting what I say,
                    but with practice you'll be able to build your own sentences.</li><br>
            <li><strong>CS 235: Object-Oriented Programming with C++</strong><br> You're learning more about software development practices,
                    design, testing, as well as more advanced object oriented concepts.</li><br>
            <li><strong>CS 250: Basic Data Structures with C++</strong><br> You're learning about data, how to store data, how to
                    assess how efficient algorithms are. Data data data.</li>
          </ul>

          <p>
            In addition to learning about the language itself, I also try to
            sprinkle in other things I've learned from experience that I think
            you should know as a software developer (or someone who codes for
            whatever reason), like
          </p>
          
          <ul>
            <li>   How do you validate that what you wrote <em>actually works?</em> <br>
                   (Spoilers: How to write tests, both manual and automated.)       </li>
            <li>   What tools can you use to make your programming life easier?     <br>
                   (And are used in the professional world?)                        </li>
            <li>   How do you design a solution given just some requirements?       </li>
            <li>   How do you network in the tech field?                            </li>
            <li>   How do you find jobs, even?                                      </li>
            <li>   What are some issues facing tech fields today?                   </li>
          </ul>                                                                     </li>
          
          <p>
            Something to keep in mind is that, if you're studying <strong>Computer Science</strong>
            as a degree (e.g., my Bachelor's degree is in Computer Science),
            technically that field is about <em>"how do computers work?"</em>,
            not about <span class="footnote" title="I know that's not grammatical but I live on the internet and this is basically what I would tweet."><em>"how do I write software good?"</em></span>, 
            but I still find these topics important to go over.
          </p>
          
          <p class="center">
            <img src="images/search.png" title="A drawing of a fake search engine where the search box says 'How 2B gud coder?'">
          </p>
          
          <br><br>
            
          <p>
            That's all I can really think of to write here.
            If you have any questions, let me know. Maybe I'll
            add on here.
          </p>
          
          <div class="back-to-top">
            <a href="#top">Back to top</a> <br> <a href="../index.html">Back to index</a>
          </div>
          
          <hr>
          
          <div class="license">
            <p>Written by Rachel Wil Sha Singh</p>
            <p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</p>
          </div>
        </div></div> <!-- <div class="row"> -->
        <!-- === END BOOK CONTENTS === -->

        <section class="">
        </section> <!-- footer -->
    </div> <!-- container-fluid -->
</body>
