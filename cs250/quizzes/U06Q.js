$( document ).ready( function() {

/*
 * Hi there. Yes, I know you can view the answers here. The quizzes
 * are meant to help you learn and practice ideas, vocab, etc.,
 * and you can take it as much as you want, so like... I guess you
 * can look at the answers, that's fine, but it's not really necessary?
 * */

quizInfo = [

  {
    "type" : "multiple-choice",
    "question" : "Determine the state of a <strong>queue</strong> after the following commands: <ol><li><code>Push(\"A\")</code></li><li><code>Push(\"B\")</code></li><li><code>Push(\"C\")</code></li></ol>",
    "more-info" : "Queues are First In First Out",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>C</td><td>B</td><td>A</td><th>Back</th></table>", "key" : "d1" },
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>A</td><td>B</td><td>C</td><th>Back</th></table>", "key" : "d2" },
    ],
    "solution" : "d2",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Determine the state of a <strong>stack</strong> after the following commands: <ol><li><code>Push(\"A\")</code></li><li><code>Push(\"B\")</code></li><li><code>Push(\"C\")</code></li></ol>",
    "more-info" : "Stacks are First In Last Out",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<table class='stack' border=1><tr><th>Top</th></tr><tr><td>C</td></tr><tr><td>B</td></tr><tr><td>A</td></tr><tr><th>Bottom</th></table>", "key" : "d1" },
      { "text" : "<table class='stack' border=1><tr><th>Top</th></tr><tr><td>A</td></tr><tr><td>B</td></tr><tr><td>C</td></tr><tr><th>Bottom</th></table>", "key" : "d2" },
    ],
    "solution" : "d1",
    "width" : 6
  },

  {
    "type" : "multiple-choice",
    "question" : "Determine the state of a <strong>queue</strong> after the following commands: <ol><li><code>Push(\"A\")</code></li><li><code>Push(\"B\")</code></li><li><code>Push(\"C\")</code></li><li><code>Push(\"D\")</code></li><li><code>Pop()</code></li></ol>",
    "more-info" : "When the Push functions are done we have: (front) A, B, C, D (back) Once we run Pop, it removes the item at the front of the queue, so then we have: B, C, D.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>C</td><td>B</td><td>A</td><th>Back</th></table>", "key" : "d1" },
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>A</td><td>B</td><td>C</td><th>Back</th></table>", "key" : "d2" },
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>B</td><td>C</td><td>D</td><th>Back</th></table>", "key" : "d3" },
      { "text" : "<table class='queue' border=1><tr><th>Front</th><td>D</td><td>C</td><td>B</td><th>Back</th></table>", "key" : "d4" },
    ],
    "solution" : "d3",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Determine the state of a <strong>stack</strong> after the following commands: <ol><li><code>Push(\"A\")</code></li><li><code>Push(\"B\")</code></li><li><code>Push(\"C\")</code></li><li><code>Push(\"D\")</code></li><li><code>Pop()</code></li></ol>",
    "more-info" : "When the Push functions are done we have: (bottom) A, B, C, D (top) Once we run Pop, it removes the top-most item, so then we have A, B, C",
    "resources" : [ ],
    "options" :
    [
      { "text" : "<table class='stack' border=1><tr><th>Top</th></tr><tr><td>C</td></tr><tr><td>B</td></tr><tr><td>A</td></tr><tr><th>Bottom</th></table>", "key" : "d1" },
      { "text" : "<table class='stack' border=1><tr><th>Top</th></tr><tr><td>B</td></tr><tr><td>C</td></tr><tr><td>D</td></tr><tr><th>Bottom</th></table>", "key" : "d2" },
      { "text" : "<table class='stack' border=1><tr><th>Top</th></tr><tr><td>D</td></tr><tr><td>C</td></tr><tr><td>B</td></tr><tr><th>Bottom</th></table>", "key" : "d3" },
      { "text" : "<table class='stack' border=1><tr><th>Top</th></tr><tr><td>A</td></tr><tr><td>B</td></tr><tr><td>C</td></tr><tr><th>Bottom</th></table>", "key" : "d4" },
    ],
    "solution" : "d1",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given this <strong>stack</strong>, how many times would we need to call Pop() in order to have the next-accessible item be \"Telangana\"?<br><div class='center'><table class='stack' border=1><tr><th>Top</th></tr><tr><td>\"Uttarakhand\"</td></tr><tr><td>\"Kerala\"</td></tr><tr><td>\"Telangana\"</td></tr><tr><td>\"Gujarat\"</td></tr><tr><th>Bottom</th></table></div>",
    "more-info" : "After the first pop we would have: Kerala, Telangana, Gujarat. After the second pop we have: Telangana, Gujarat.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "1", "key" : "d1" },
      { "text" : "2", "key" : "d2" },
      { "text" : "3", "key" : "d3" },
      { "text" : "4", "key" : "d4" },
    ],
    "solution" : "d2",
    "width" : 6
  },
  
  {
    "type" : "multiple-choice",
    "question" : "Given this <strong>queue</strong>, how many times would we need to call Pop() in order to have the next-accessible item be \"Kabe\"?<br><div class='center'><table class='queue' border=1><tr><th>Front</th><td>Korra</td><td>Pixel</td><td>Luna</td><td>Kabe</td><th>Back</th></table></div>",
    "more-info" : "After the first pop we would have: Pixel, Luna, Kabe. After the second pop we would have: Luna, Kabe. After the third pop we would have: Kabe.",
    "resources" : [ ],
    "options" :
    [
      { "text" : "1", "key" : "d1" },
      { "text" : "2", "key" : "d2" },
      { "text" : "3", "key" : "d3" },
      { "text" : "4", "key" : "d4" },
    ],
    "solution" : "d3",
    "width" : 6
  },
  

  //{ // TEMPLATE MULTIPLE CHOICE
    //"type" : "multiple-choice",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"options" :
    //[
    // { "text" : "", "key" : "" },
    //],
    //"solution" : "",
    //"width" : "12"
  //},

  //{ // TEMPLATE DROPDOWN
    //"type" : "dropdown",
    //"question" : "",
    //"more-info" : "",
    //"resources" : [ ],
    //"dropdowns" :
    //[
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
      //{
        //"width" : 12,
        //"prompt" : "",
        //"options" :
        //[
        //],
        //"solution" : ""
      //},
    //]
  //},

];

} );
