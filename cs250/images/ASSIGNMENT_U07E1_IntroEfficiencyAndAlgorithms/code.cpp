#include <iostream>
using namespace std;


void RunRace( int n )
{
  cout << "RunRace(" << n << ")... ";
  int iterationCounter = 0;
  
  //cout << "Distance: " << distance << endl;
  
  while ( n > 0 )
  {
    n = n / 2;
    
    //cout << "Distance: " << distance << endl;
    
    iterationCounter++;
  }
  
  cout << iterationCounter << " iterations" << endl;
}

void Line( int n )
{
  cout << "Line(" << n << ")... ";
  int iterationCounter = 0;
  
  for ( int i = 0; i < n; i++ )
  {
    //cout << "-";
    iterationCounter++;
  }
  
  cout << iterationCounter << " iterations" << endl;
}

void Square( int n )
{
  cout << "Square(" << n << ")... ";
  int iterationCounter = 0;
  
  for ( int y = 0; y < n; y++ )
  {
    for ( int x = 0; x < n; x++ )
    {
      //cout << "*";
      iterationCounter++;
    }
    //cout << endl;
  }
  
  cout << iterationCounter << " iterations" << endl;
}

void Cube( int n )
{
  cout << "Cube(" << n << ")... ";
  int iterationCounter = 0;
  
  for ( int z = 0; z < n; z++ )
  {
    for ( int y = 0; y < n; y++ )
    {
      for ( int x = 0; x < n; x++ )
      {
        //cout << "(" << x << "," << y << "," << z << ")\t";
        
        iterationCounter++;
      }
    }
  }
  
  cout << iterationCounter << " iterations" << endl;  
}

void DrawSomething( int height )
{
  int iterationCounter = 0;
  
  for ( int y = 0; y < height; y++ )
  {
    for ( int x = 0; x < y; x++ )
    {
      cout << "*";
      iterationCounter++;
    }
    cout << endl;
  }
  
  cout << iterationCounter << " iterations" << endl;
}

void UselessFunction( int n )
{
  int iterationCounter = 0;
  
  while ( n > 0 )
  {
    n--;
    for ( int i = 0; i < n; i++ )
    {
      cout << n << " ";
      iterationCounter++;
    }
  }
  
  cout << endl;
  cout << iterationCounter << " iterations" << endl;
}

int Fib( int n, int& iterationCounter )
{
  iterationCounter++;
  if ( n == 0 || n == 1 ) { return 1; }
  return Fib( n-1, iterationCounter ) + Fib( n-2, iterationCounter );
}

int main()
{
  cout << string( 80, '.' );
  cout << endl << "FIBONACCI" << endl;
  int iterationCounter = 0;
  Fib( 1, iterationCounter );
  cout << "Fib(1) = " << iterationCounter << endl;
  
  iterationCounter = 0;
  Fib( 2, iterationCounter );
  cout << "Fib(2) = " << iterationCounter << endl;
  
  iterationCounter = 0;
  Fib( 3, iterationCounter );
  cout << "Fib(3) = " << iterationCounter << endl;
  
  iterationCounter = 0;
  Fib( 4, iterationCounter );
  cout << "Fib(4) = " << iterationCounter << endl;
  
  iterationCounter = 0;
  Fib( 5, iterationCounter );
  cout << "Fib(5) = " << iterationCounter << endl;
  
  iterationCounter = 0;
  Fib( 10, iterationCounter );
  cout << "Fib(10) = " << iterationCounter << endl;
  
  iterationCounter = 0;
  Fib( 100, iterationCounter );
  cout << "Fib(100) = " << iterationCounter << endl;
  
  
  
  
  cout << string( 80, '.' );
  cout << endl << "RUN RACE" << endl;
  RunRace( 1 );
  RunRace( 2 );
  RunRace( 4 );
  RunRace( 10 );
  RunRace( 100 );
  RunRace( 1000 );
  
  cout << string( 80, '.' );
  cout << endl << "LINE" << endl;
  Line( 1 );
  Line( 2 );
  Line( 4 );
  Line( 10 );
  Line( 100 );
  Line( 1000 );
  
  cout << string( 80, '.' );
  cout << endl << "SQUARE" << endl;
  Square( 1 );
  Square( 2 );
  Square( 4 );
  Square( 10 );
  Square( 100 );
  Square( 1000 );
  
  cout << string( 80, '.' );
  cout << endl << "CUBE" << endl;
  Cube( 1 );
  Cube( 2 );
  Cube( 4 ); 
  Cube( 10 );
  Cube( 100 );
  Cube( 1000 );
  
  
  
  
  /*
  cout << endl << "DRAW SOMETHING" << endl;
  DrawSomething( 1 );
  DrawSomething( 2 );
  DrawSomething( 3 );
  DrawSomething( 5 );
  DrawSomething( 10 );
  cout << string( 80, '.' );
  cout << endl << "USELESS FUNCTION" << endl;
  UselessFunction( 2 );
  UselessFunction( 4 );
  UselessFunction( 6 );
  UselessFunction( 8 );
  UselessFunction( 10 );
  cout << string( 80, '.' );
  * */
  
  return 0;
}
