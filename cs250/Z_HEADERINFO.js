// CS 250
$( document ).ready( function() {

    headerInfo = {
        "course"        : "CS 250: Basic Data Structures using C++",
        "dates"         : "2022-01-18 to 2022-05-16",
        "semester"      : "Spring 2022",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs235/",
        "textbook1"     : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_250",
        "syllabus"      : "syllabus.html",
        "exampleCode"   : "https://gitlab.com/rachels-courses/example-code/-/tree/main/C%2B%2B",
        "prerequisites" : "CS 235 or (CS 200 and CS 210 or CS 236 or CS 255 or CIS 240 or MATH 242).",
        "description"   : "This course continues developing problem solving techniques by focusing on object-oriented styles using C++ abstract data types. Basic data structures such as queues, stacks, trees, dictionaries, their associated operations, and their array and pointer implementations will be studied. Topics also include recursion, templates, fundamental algorithm analysis, searching, sorting, hashing, object-oriented concepts and large program organization. Students will write programs using the concepts covered in the lecture. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
        "credit_hours"  : "4",
    };
    
    sectionInfo = [
        {
            "semester"      : "Spring 2022",
            "crn"           : "11489",
            "section"       : "377",
            "method"        : "Hybrid",
            "times"         : "Tues, 6:00 - 8:50 pm",
            "background"    : "bg1",
            "room"          : "RC 380"
        }
    ];
    
} );
