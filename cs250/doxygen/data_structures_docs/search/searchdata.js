var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuw~",
  1: "abcdhilmnpqst",
  2: "bhimqrs",
  3: "abchilmnpqrst",
  4: "abcdefghilmnopqrstw~",
  5: "acdhkmnprstu",
  6: "c",
  7: "dlq",
  8: "bhlqs",
  9: "el",
  10: "cd"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros",
  10: "Pages"
};

