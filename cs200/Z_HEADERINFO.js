// CS 200
$( document ).ready( function() {

    headerInfo = {
        "courseCode"    : "CS 200",
        "course"        : "CS 200: Concepts of Programming with C++",
        "dates"         : "2022-01-18 to 2022-05-16",
        "semester"      : "Spring 2022",
        "homepage"      : "https://rachels-courses.gitlab.io/webpage/cs200/",
        "textbook1"     : "https://gitlab.com/RachelWilShaSingh/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf?inline=false",
        "catalog"       : "http://catalog.jccc.edu/coursedescriptions/cs/#CS_200",
        "syllabus"      : "syllabus.html",
        "exampleCode"   : "https://gitlab.com/rachels-courses/example-code/-/tree/main/C%2B%2B",
        "prerequisites" : "CS 134 with a grade of 'B' or higher or CIS 142 with a grade of 'B' or higher or CS 201 or CS 205 or MATH 241 or an appropriate score on department waiver test or department approval.",
        "description"   : "This course emphasizes problem solving using a high level programming language and the software development process. Algorithm design and development, programming style, documentation, testing and debugging will be presented. Standard algorithms and data structures will be introduced. Data abstraction and an introduction to object-oriented programming will be studied and used to implement algorithms. 3 hrs. lecture, 2 hrs. lab by arrangement/wk.",
        "credit_hours"  : "4",
    };
    
    sectionInfo = [
        {
            "semester"      : "Spring 2022",
            "crn"           : "11468",
            "section"       : "350",
            "method"        : "Online only",
            "times"         : "Online only",
            "background"    : "bg1",
            "room"          : "none"
        },
        {
            "semester"      : "Spring 2022",
            "crn"           : "11474",
            "section"       : "378",
            "method"        : "Hybrid",
            "times"         : "Tues/Thurs, 4:30 - 5:45 pm",
            "background"    : "bg2",
            "room"          : "RC 344"
        }
    ];
    
} );
