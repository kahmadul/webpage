#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include "Structures.h"

void Program1_ExploringAddresses();
void Program2_DereferencingPointers();
void Program3_PointersAndClasses();
void Program4_DynamicArrays();

#endif
